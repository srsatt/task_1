//
//  AppDelegate.m
//  Task1_reutov
//
//  Created by Pavel on 27.09.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "AppDelegate.h"
#import "Animal.h"
@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    Animal *my_cat = [[Animal alloc] init];
    NSLog(@"%@", my_cat);
    
    
    Animal *my_dog = [[Animal alloc] init];
    my_dog.name = @"Sharik";
    my_dog.kind=@"dog";
    NSLog(@"%@", my_dog);
    
    Animal *my_mutan_fish=[[Animal alloc] initWithName:@"Bulk" Kind:@"Fish" WoolStatus:(TRUE) ScaleStatus:(FALSE) Bare_skinStatus:(FALSE)];
    Animal *my_mutan_dog=[[Animal alloc] initWithName:@"Wuf-Wuf" Kind:@"Dog" WoolStatus:(FALSE) ScaleStatus:(TRUE) Bare_skinStatus:(FALSE)];
    
     NSLog(@"mutants are %@ %@", my_mutan_dog,my_mutan_fish);
    
//  you don't need () around @""
//fixed
    Animal *my_parrot = [[Animal alloc] initWithName: @"yasha" Kind: @"parrot" WoolStatus: (TRUE) ScaleStatus: (TRUE) Bare_skinStatus: (TRUE) ];
    NSLog(@"%@", my_parrot);
    
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}



@end
