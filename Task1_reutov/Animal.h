//
//  Animal.h
//  Task1_reutov
//
//  Created by Павел on 27.09.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Animal : NSObject

//  TODO: add couple if setters
@property BOOL isWool;
@property BOOL isScales;
@property BOOL isBare_skin;
@property NSString *name;
@property NSString *kind;
- (instancetype)initWithName: (NSString*) Name Kind: (NSString*) Kind WoolStatus: (BOOL) WoolStatus ScaleStatus: (BOOL) ScaleStatus Bare_skinStatus: (BOOL) Bare_skinStatus;

@end
