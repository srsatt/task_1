//
//  Animal.m
//  Task1_reutov
//
//  Created by Павел on 27.09.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "Animal.h"

@implementation Animal

- (instancetype)init{
    if (!(self = [super init]))  return nil;
    _name = @"IT";
    _isWool = NO;
    _isScales= YES;
    _isBare_skin =NO;
   
    return self;
}

- (instancetype)initWithName: (NSString*) Name Kind: (NSString*) Kind WoolStatus: (BOOL) WoolStatus ScaleStatus: (BOOL) ScaleStatus Bare_skinStatus: (BOOL) Bare_skinStatus {
    if (WoolStatus +ScaleStatus+Bare_skinStatus > 1) return nil;
    if (!(self = [self init]))  return nil;
    
    if ([Kind isEqualToString:@"Fish"] && (WoolStatus||Bare_skinStatus)) return nil;
     if ([Kind isEqualToString:@"Dog"] && (ScaleStatus)) return nil;
    
    
    _name=Name;
    _kind=Kind;
    _isWool = WoolStatus;
    _isScales= ScaleStatus;
    _isBare_skin =Bare_skinStatus;
    return self;
}



- (NSString *)description{

    assert(self.name);
    NSString *cover;
    
    if (self.isWool == TRUE) cover=@"Wool";
    if(self.isScales==TRUE) cover=@"Scales";
    if(self.isBare_skin==YES) cover=@"Bare_skin";
    
    return [NSString stringWithFormat:@" \n This fnimal is a %@ \n and fnimal's name is %@ \n and cover is  %@",self.kind, self.name, cover];
}

@end

